#!/bin/sh

#
# Copyright 2023 René Panke
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

DOWNLOAD_URL_CLIP="https://gitlab.com/renepanke/clipit/-/jobs/artifacts/1.0.1/raw/clip?job=build"
DOWNLOAD_URL_CLIPF="https://gitlab.com/renepanke/clipit/-/jobs/artifacts/1.0.1/raw/clipf?job=build"
INSTALL_DIR="/opt/clipit"

printf "🔨 Creating %s ...\n" "$INSTALL_DIR"
sudo mkdir -p "$INSTALL_DIR"
printf "✅ Created %s !\n" "$INSTALL_DIR"

printf "📥 Download clipit...\n"
sudo curl -fsSL -o "$INSTALL_DIR/clip" "$DOWNLOAD_URL_CLIP"
sudo curl -fsSL -o "$INSTALL_DIR/clipf" "$DOWNLOAD_URL_CLIPF"
printf "✅ Downloaded clipit!\n"

sudo chmod +x "$INSTALL_DIR/clip" "$INSTALL_DIR/clipf"

printf "🔨 Adding clipit to PATH...\n"
printf "export PATH=%s:\$PATH\n" "$INSTALL_DIR" >> ~/.profile
. ~/.profile
printf "✅ Added clipit to PATH !\n"
printf "🚀🚀🚀 Finished installation of clipit! Try by executing \"clip -h\" or \"clipf -h\"! 🚀🚀🚀\n"
