/*
 * Copyright 2023 René Panke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use std::env;
use std::io::{Read, stdin};

use arboard::Clipboard;

fn main() {
    const VERSION: &str = env!("CARGO_PKG_VERSION");

    let args: Vec<String> = env::args().collect();

    if args.len() == 2 && (&args[1] == "--help" || &args[1] == "-h") {
        println!("clipit {} | clip: A command of the clipit bundle to pipe outputs of other commands to for copying it to the clipboard", VERSION);
        println!("Usage: echo \"abc\" | clip");
        std::process::exit(0);
    }

    let mut input = String::new();
    stdin().read_to_string(&mut input).expect("CLIP_ERR_0001: Failed to read from STDIN.");
    Clipboard::new().expect("CLIP_ERR_0002: Failed to create a new Clipboard object.")
        .set_text(input).expect("CLIP_ERR_0003: Failed to set clipboard content.");
}