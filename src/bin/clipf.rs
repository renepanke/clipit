/*
 * Copyright 2023 René Panke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use std::env;
use std::fs::File;
use std::io::Read;

use arboard::Clipboard;

fn main() {
    const VERSION: &str = env!("CARGO_PKG_VERSION");

    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("clipit {} | clipf: A command of the clipit bundle to copy file contents to the clipboard", VERSION);
        eprintln!("Usage: clipf <file_path>");
        std::process::exit(1);
    }

    if &args[1] == "--help" || &args[1] == "-h" {
        println!("clipit {} | clipf: A command of the clipit bundle to copy file contents to the clipboard", VERSION);
        println!("Usage: clipf <file_path>");
        std::process::exit(0);
    }

    let file_path = &args[1];

    let mut file = File::open(file_path).expect("CLIPF_ERROR_0001: Could not open file.");

    let mut file_content = String::new();
    file.read_to_string(&mut file_content).expect("CLIPF_ERROR_0002: Could not read file contents.");

    Clipboard::new().expect("CLIPF_ERR_0003: Failed to create a new Clipboard object.")
        .set_text(file_content).expect("CLIPF_ERR_0004: Failed to set clipboard content.");
}
