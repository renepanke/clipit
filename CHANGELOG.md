# Changelog

## 1.0.0

- Initial Release

# 1.0.1

- Improved help message, now containing version and program name
- Added Windows installation description to README