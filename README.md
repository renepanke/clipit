# clipit

A bundle of command line tools to copy contents to the clipboard free from bullshit.

## Usage / Examples

### clip

```bash
# Copy the string "abc" to the clipboard
echo "abc" | clip

# Copy some other command output to the clipboard
cat somefile.txt | grep -E "someregex" | clip
```

### clipf

```bash
# Copy the raw text contents of a file to the clipboard
clipf somefile.txt
```

## Installation

### Linux

```shell
curl -fsSL https://gitlab.com/renepanke/clipit/-/raw/master/src/install/linux/install.sh | sh
source ~/.profile
```

### Windows

Download the `clip` and `clipf` binary from the [Releases Page](https://gitlab.com/renepanke/clipit/-/releases) and
paste them into a folder. Add this folder to the `PATH` environment variable.
Proceed by recreating you shell.

## Authors

- [@renepanke](https://gitlab.com/renepanke)